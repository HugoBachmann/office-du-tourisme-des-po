$(document).ready(function () {
    
    // INIT AOS
    AOS.init();

    // Menu mobile
    $('.menu_burger').unbind('click').bind('click', function(e){
        e.preventDefault();
        $('.menu_burger').toggleClass('bleu');
        $('.mobile_menu').slideToggle();
    })

    // Menu admin

    // Gestion
    $('.btn_gestion').unbind('click').bind('click', function(e){
        e.preventDefault();
        $('.content_admin_menu_gestion').slideToggle();

        $('.btn_gestion').toggleClass('vert');
        $('.dropdown_gestion').toggleClass('rotate_btn');
    })

    // // Edit Rent Pro
    // $('.btn_open_edit').unbind('click').bind('click', function(e){
    //     $('#edit_rent_pro').fadeIn()
    // })

    // $('.btn_close_edit').unbind('click').bind('click', function(e){
    //     $('#edit_rent_pro').fadeOut()
    // })

    // Validation
    $('.btn_validation').unbind('click').bind('click', function(e){
        e.preventDefault();
        $('.content_admin_menu_validation').slideToggle();

        $('.btn_validation').toggleClass('rouge');
        $('.dropdown_validation').toggleClass('rotate_btn');
    })

    // Contact
    $('.btn_contact').unbind('click').bind('click', function(e){
        e.preventDefault();
        $('.content_admin_menu_contact').slideToggle();

        $('.btn_contact').toggleClass('bleu');
        $('.dropdown_contact').toggleClass('rotate_btn');
    })

    // Ajax Backoffice

    // Retour acceuil backoffice
    $('.administration').unbind('click').bind('click', function(){
        $.get('/admin/infoAdmin', function(data){
            $('.admin_content').html(data);
        })
    })

    // Chargement par defaut du contenu accueil backoffice
    $( ".admin_content" ).load( "/admin/infoAdmin" );

    // Validate Pro
    $('.pro_validation').unbind('click').bind('click', function(){
        $.get('/admin/validatePro', function(data){
            $('.admin_content').html(data);
        })
    })

    // Validate Rent
    $('.validation_post').unbind('click').bind('click', function(){
        $.get('/admin/validateRent', function(data){
            $('.admin_content').html(data);
        })
    })

    // Add Rent 
    $('.add_rent').unbind('click').bind('click', function(){
        $.get('/addRent', function(data){
            $('.admin_content').html(data);
        })
    })

    // Liste Rent
    $('.cat_rent').unbind('click').bind('click', function(){
        $.get('/admin/listeRent', function(data){
            $('.admin_content').html(data);
            rentUnique();
        })
    })

      // Contact Admin
    $('.contact_form').unbind('click').bind('click', function(){
        $.get('/admin/contactAdmin', function(data){
            $('.admin_content').html(data);
        })
    })

    // Liste membre
    $('.cat_member').unbind('click').bind('click', function(){
        $.get('/admin/listeMembre', function(data){
            $('.admin_content').html(data);
            membreUnique();
        })
    })

     // Add Membre
     $('.add_member').unbind('click').bind('click', function(){
        $.get('/admin/addMember', function(data){
            $('.admin_content').html(data);
        })
    })

    function membreUnique(){
        // Membre unique
        $('.btn_individual').unbind('click').bind('click', function(){
            let link = $(this).data('link');
            $.get(link, function(data){
                $('.admin_content').html(data);
            })   
        })
    }

    function rentUnique(){
        // Membre unique
        $('.btn_single').unbind('click').bind('click', function(){
            let link = $(this).data('link');
            $.get(link, function(data){
                $('.admin_content').html(data);
            })   
        })
    }

});

