mapboxgl.accessToken = 'pk.eyJ1IjoibDN4d3giLCJhIjoiY2ttejF4dTdnMDlrNzJzcGZna2l2Y3NtZiJ9.tuq1-O3R39DFglmwd8-1TQ';

var mapboxClient = mapboxSdk({
    accessToken: mapboxgl.accessToken
})

var map = new mapboxgl.Map({
    container: 'map_container', // container ID
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [2.9, 42.7], // starting position [lng, lat]
    zoom: 10 // starting zoom
});


// var geojson = {
//     type: 'FeatureCollection',
//     features: [{
//       type: 'Feature',
//       properties: {
//         title: 'Mapbox',
//         description: 'Le soler, 66270',
//         adress: '19 rue des oiseaux le soler'
//       }
//     },
//     {
//       type: 'Feature',
//       properties: {
//         title: 'Mapbox',
//         description: 'Collioure, 66190',
//         adress: '11 rue René Laennec Saint Laurent de la Salanque'
//       }
//     }]
//   };

    // INFO RENT MAP
    $.get('/getmapmarkers', function(data) {
        data = JSON.parse(data)

        data.forEach( function (marker) {
                var markerDiv = document.createElement('div');
            markerDiv.className = 'marker';
            // On transforme l'adresse du marqueur pour la convertir en longitude/latitude
            mapboxClient.geocoding.forwardGeocode({
                    query: marker.adresse,
                    autocomplete: false,
                    limit: 1
                })
                .send()
                .then(function(response) {
                    if (response && response.body && response.body.features && response.body.features.length) {
                        var feature = response.body.features[0];

                    new mapboxgl.Marker(markerDiv)
                        .setLngLat(feature.center)
                        .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                        .setHTML('<h3>' + marker.type + '</h3><p>' + marker.description + '</p>' + '<br>' + '<a class="lien_popup btn btn-primary" href="/locations">Voir plus</a>'))
                        .addTo(map);
                }
            });
        })
    })

