<?php 

namespace App\Repositories;

use App\Models\Contact;

class ContactRepository extends Repository
{
    public function getTable(): string { return 'contact'; }

    // Enregistrer les message
    public function contactPost( string $user_name, string $user_email, string $user_msg) 
    {
        $user_name=$_POST['user_name'];
        $user_email=$_POST['user_email'];
        $user_msg=$_POST['user_msg'];
        
        if(isset($_POST['user_name']) && isset($_POST['user_email']) && isset($_POST['user_msg'])){
            $q = 'INSERT INTO `contact` (`user_name`, `user_email`, `user_msg`) VALUES ( :user_name, :user_email, :user_msg)';
        
            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'user_name' => $user_name,
                'user_email' => $user_email,
                'user_msg' => $user_msg
            ]);
        } else {
            
            echo 'Tous les champs doivent être remplis !';
        }

    }

    // Suppr les message
    public function contactDelete( $id )
    {
        $q = 'SELECT * FROM  `contact` WHERE `id`=:id ';
        $stmt = $this->pdo->prepare( $q );

        $stmt->execute([
            'id' => $id 
        ]);

        if ($stmt->rowCount() == 0) {
            return null;
            
        } else {

            $q = 'DELETE FROM `contact` WHERE `id`=:id';
            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'id' => $id 
            ]);
        }
    }
    
    // RECUPERE TOUS LES CONTACT
    public function findAll(): array
    {
        return $this->readAll( Contact::class );
    }
}