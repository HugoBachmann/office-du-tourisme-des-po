<?php

namespace App\Repositories;

use PDO;
use Laminas\Diactoros\Response\HtmlResponse;

use App\App;
use App\Database;
use App\Models\Model;
use App\Models\User;
use App\Utils;
use mysqli;

abstract class Repository
{
    abstract public function getTable(): string;

    public ?PDO $pdo = null;

    public function __construct()
    {
        $this->pdo = Database::getInstance()->getPdo();

        if( is_null( $this->pdo ) ) {
            App::getInstance()
                ->getRouter()
                ->getPublisher()
                ->publish( new HtmlResponse( 'Database server error.', 500 ) );

            die();
        }
    }

    protected function readAll( string $class_name ): array
    {
        $result = [];

        $q = sprintf( 'SELECT * FROM  `%s`', $this->getTable() );
        $stmt = $this->pdo->query( $q );

        if ( $stmt ) {
            foreach( $stmt as $row ) {
                $rent = new $class_name( $row );
                $result[] = $rent;
            }
        }

        return $result;
    }

    protected function readById( string $class_name, int $id ): ?Model
    {
        $q = sprintf( 'SELECT * FROM  `%s` WHERE id=:id', $this->getTable() );
        $stmt = $this->pdo->prepare( $q );
        $stmt->execute( [ 'id' => $id ] );

        if ( $stmt ) {
            foreach( $stmt as $row ) {
                $rent = new $class_name( $row );

                return $rent;
            }
        }

        return null;
    }

    public function findAllByColumnValue(string $tableName, string $columnName, $columnValue): array
    {
        $result = [];

        $query="SELECT * FROM `users` WHERE `$columnName`=:$columnName";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            $columnName => $columnValue
        ]);

        if ($statement) {
            foreach ($statement as $row) {
                $model = new $tableName($row);
                $result[] = $model;
            }
        }

        return $result;
    }

    public function findAllByStatus (string $tableName, string $columnName, $columnValue): array
    {
        $result = [];

        $query="SELECT * FROM `rental` WHERE `$columnName`=:$columnName";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            $columnName => $columnValue
        ]);

        if ($statement) {
            foreach ($statement as $row) {
                $model = new $tableName($row);
                $result[] = $model;
            }
        }

        return $result;
    }


}