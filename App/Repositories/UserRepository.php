<?php

namespace App\Repositories;

use App\Models\User;
use App\Utils;


class UserRepository extends Repository
{
    public function getTable(): string { return 'users'; }

    // Auth
    public function auth( string $email, string $password ): ?User
    {
        $q = 'SELECT * FROM  `users` WHERE `email`=:email AND `password`=:password';
        $stmt = $this->pdo->prepare( $q );

        $stmt->execute([
            'email' => $email,
            'password' => Utils::passwordHash( $password )
        ]);

        if ( $stmt && $stmt->rowCount() > 0 ) {
            $user = new User( $stmt->fetch() );
            $user->password = '';

            return $user;
        }

        return null;
    }

    // Delete
    public function deleteUser ( $id )
    {
        $q = 'SELECT * FROM  `users` WHERE `id`=:id ';
        $stmt = $this->pdo->prepare( $q );

        $stmt->execute([
            'id' => $id 
        ]);

        if ($stmt->rowCount() == 0) {
            return null;
            
        } else {

            $q = 'DELETE FROM `users` WHERE `id`=:id';
            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'id' => $id 
            ]);
        }
    }

    // Register  POST
    public function reg( string $firstname, string $lastname, string $email, string $password, int $pro_request): void 
    {   
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        // $is_admin = intval($_POST['is_admin']);
        // $is_host = intval($_POST['is_host']);
        $pro_request = intval($_POST['pro_request']);

        if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['pro_request']) ){

            $q = 'INSERT INTO `users` (`firstname`, `lastname`, `email`, `password`, `is_admin`, `is_host` , `pro_request`) VALUES ( :firstname, :lastname, :email, :password, :is_admin, :is_host, :pro_request)';
            var_dump($q);

            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'password' => Utils::passwordHash( $password ),
                'is_admin' => 0,
                'is_host' => 0,
                'pro_request' =>  $pro_request,
            ]);

        } else {

            echo 'Tous les champs doivent être remplis !';
        }
        
    }

    // Add User
    public function addUserPost ( string $firstname, string $lastname, string $email, string $password, int $is_admin, int $is_host ): void 
    {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $is_admin = intval($_POST['is_admin']);
        $is_host = intval($_POST['is_host']);

        if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password']) ){

            $q = 'INSERT INTO `users` (`firstname`, `lastname`, `email`, `password`, `is_admin`, `is_host`) VALUES ( :firstname, :lastname, :email, :password, :is_admin, :is_host)';
            var_dump($q);

            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'password' => Utils::passwordHash( $password ),
                'is_admin' => $is_admin,
                'is_host' =>  $is_host,
            ]);

        } else {

            echo 'Tous les champs doivent être remplis !';
        }
    }

    // Update User
    public function updateUser ( string $firstname, string $lastname, string $email, string $password, string $is_admin, string $is_host, $id): void 
    {
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $is_admin = intval($_POST['is_admin']);
        $is_host = intval($_POST['is_host']);
        $id = $_POST['id'];

        if (isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['id']) ){

            $q = 'UPDATE `users` SET firstname = :firstname, lastname = :lastname, email =:email, password =:password, is_admin = :is_admin, is_host =:is_host WHERE `id` = :id';
            var_dump($q);

            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'password' => Utils::passwordHash( $password ),
                'is_admin' => $is_admin,
                'is_host' =>  $is_host,
                'id' =>  $id
            ]);

        } else {

            echo 'Tous les champs doivent être remplis !';
        }
    }

    // Cherche les users en attente de pro
    public function findAllUsersWaitingForApproval(): array
    {
        return $this->findAllByColumnValue(User::class, "pro_request", 1);
    }

    // Valide les users Pro 
    public function validatePro ($id)
    {
        
        $user = $this->findById(intval($id));
        if ($user->is_host == 0) {
            $q = "UPDATE `users` SET is_host = '1', pro_request= '0' WHERE `id`=:id";

            $stmt = $this->pdo->prepare($q);

            $stmt->execute([
                "id" => $id,
            ]);
        } 
    }

     // Valide les users Pro 
     public function deniedPro ($id)
     {
         
         $user = $this->findById(intval($id));
         if ($user->is_host == 0) {
             $q = "UPDATE `users` SET is_host = '0', pro_request= '0' WHERE `id`=:id";
 
             $stmt = $this->pdo->prepare($q);
 
             $stmt->execute([
                 "id" => $id,
             ]);
         } 
     }

    // Lecture de toutes les lignes
    public function findAll(): array
    {
        return $this->readAll( User::class );
    }
    // Lecture par ID
    public function findById( int $id ): ?User
    {
        return $this->readById( User::class, $id );
    }
}