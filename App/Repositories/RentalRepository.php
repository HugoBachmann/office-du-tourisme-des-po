<?php

namespace App\Repositories;

use App\Models\Rental;
use App\Utils;

class RentalRepository extends Repository
{
    public function getTable(): string { return 'rental'; }

    // ADD RENT POST
    public function addRent(string $type, int $owner_id, string $adresse, int $nbr_room, string $equipement, string $description, float $price) :void
    {
        $type = $_POST['type'];
        $adresse = $_POST['adresse'];
        $nbr_room = intval($_POST['nbr_room']);
        $equipement = $_POST['equipement'];
        $description = $_POST['description'];
        $price = floatval($_POST['price']);

        if (isset($_POST['type']) && isset($_POST['adresse']) && isset($_POST['nbr_room']) && isset($_POST['equipement'])  && isset($_POST['description'])  && isset($_POST['price'])){

            $q = 'INSERT INTO `rental` (`type`, `owner_id`, `adresse`, `nbr_room`, `equipement`, `description` , `price`, `rent_request`, `rent_validate`) VALUES ( :type, :owner_id, :adresse, :nbr_room, :equipement, :description, :price, :rent_request, :rent_validate)';
            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'type' => $type,
                'owner_id' => $owner_id,
                'adresse' => $adresse,
                'nbr_room' => $nbr_room,
                'equipement' => $equipement,
                'description' => $description,
                'price' =>  $price,
                'rent_request' => 1,
                'rent_validate' => 0
            ]);

                header( 'location: /admin');

        } else {

                echo 'Tous les champs doivent être remplis !';
        }
    }

    // DELETE RENT
    public function deleteRent ($id)
    {
        $q = 'SELECT * FROM  `rental` WHERE `id`=:id ';
        $stmt = $this->pdo->prepare( $q );

        $stmt->execute([
            'id' => $id 
        ]);

        if ($stmt->rowCount() == 0) {
            return null;
            
        } else {

            $q = 'DELETE FROM `rental` WHERE `id`=:id';
            $stmt = $this->pdo->prepare( $q );

            $stmt->execute([
                'id' => $id 
            ]);
        }
    }

    // UPDATE RENT
    public function updateRent(string $type, int $owner_id,string $adresse, int $nbr_room, string $equipement, string $description, int $price, $rent_request, $rent_validate, $id) :void
    {
        $type = $_POST['type'];
        $owner_id = $_POST['owner_id'];
        $adresse = $_POST['adresse'];
        $nbr_room = intval($_POST['nbr_room']);
        $equipement = $_POST['equipement'];
        $description = $_POST['description'];
        $price = intval($_POST['price']);
        $rent_request = $_POST['rent_request'];
        $rent_validate = $_POST['rent_validate'];
        $id = $_POST['id'];

        if (isset($_POST['type']) && isset($_POST['adresse']) && isset($_POST['nbr_room']) && isset($_POST['equipement'])  && isset($_POST['description'])  && isset($_POST['price'])){

            $q = 'UPDATE `rental` SET type = :type, owner_id = :owner_id, adresse =:adresse, nbr_room =:nbr_room, equipement = :equipement, description =:description, rent_request =:rent_request, rent_validate =:rent_validate WHERE `id` = :id';
           

            $stmt = $this->pdo->prepare( $q );

            var_dump($stmt);
            $stmt->execute([
                'type' => $type,
                'owner_id' => $owner_id,
                'adresse' => $adresse,
                'nbr_room' => $nbr_room,
                'equipement' => $equipement,
                'description' => $description,
                'price' =>  $price,
                'rent_request' =>  $rent_request,
                'rent_validate' =>  $rent_validate,
                'id' =>  $id
            ]);


        } else {
            echo 'Tous les champs doivent être remplis !';
        }
    }

    // VALIDATE RENT
    public function validateRent ($id)
    {
        $rent = $this->findById(intval($id));
        if ($rent->rent_validate == 0) {
            $q = "UPDATE `rental` SET rent_validate = '1', rent_request= '0' WHERE `id`=:id";

            $stmt = $this->pdo->prepare($q);

            $stmt->execute([
                "id" => $id,
            ]);
        }   
    }

    //Cherche les RENT en attente de VALIDATION
    public function findAllRentWaitingForApproval(): array
    {
        return $this->findAllByStatus(Rental::class, "rent_request", 1);
    }

    // Lecture de Toutes les lignes
    public function findAll(): array
    {
        return $this->readAll( Rental::class );
    }

    // Lecture par ID
    public function findById( int $id ): ?Rental
    {
        return $this->readById( Rental::class, $id);
    }
}