<?php

namespace App;

class Session
{
    // Authentification
    public const SESSION_USER = 'SESSION_USER';
    
    // Formulaires
    public const SESSION_FORM_STATUS = 'SESSION_FORM_STATUS';

    // Sécurité
    public const SESSION_ANTI_CSRF_TOKEN = 'SESSION_ANTI_CSRF_TOKEN';

    public static function get( string $session_name )
    {
        if( isset( $_SESSION[ $session_name ] ) ) {
            return $_SESSION[ $session_name ];
        }

        return null;
    }

    public static function set( string $session_name, $value ): void
    {
        $_SESSION[ $session_name ] = $value;
    }
}