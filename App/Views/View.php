<?php

namespace App\Views;

define('TEMPLATE_PATH', APP_PATH . 'Views' . DS . 'templates' . DS);

class View
{
    private string $templates_name;
    public bool $template_only = false;

    public function __construct(string $templates_name)
    {
        $this->templates_name = $templates_name;
    }

    public function render (array $data = []):void
    {
        extract($data);

        if(!$this->template_only){
            require_once TEMPLATE_PATH . 'partials' . DS . '_header.php';
        }

        require_once TEMPLATE_PATH . $this->templates_name . '.php';

        if(!$this->template_only){
            require_once TEMPLATE_PATH . 'partials' . DS . '_footer.php';
        }
    }
}