
<section id="home">
    <section id="landing">
        <div class="content_left">
            <div class="main_content">
                <h1 data-aos="fade-right" data-aos-duration = "600">Voyager dans les <br> pyrénnées orientales</h1>
                <p data-aos="fade-right" data-aos-duration = "600" data-aos-delay= "200">Plan and book your perfect trip with expert advice,
                    travel tips, destination information and
                    inspiration from us.
                </p>
                <a data-aos-anchor="#header" data-aos="fade-right" class="blue_btn" href="/register">Inscrivez-vous</a>
            </div>
            <!-- /.container -->
        </div>
        <!-- /.content_left -->
        <div class="content_right">
            <!-- ici l'illustration en bg -->
        </div>
        <!-- /.right_content -->
    </section> 
    <!-- /#landing -->


    <section id="discovery">
        <div class="content_left">
            <div data-aos="zoom-in-left" data-aos-duration = "800" class="img_content train_jaune">
                <span>Petit train jaune</span>
            </div>
            <!-- /.img_content -->
            <div data-aos="zoom-in-up" data-aos-duration = "800" data-aos-delay= "400" class="img_content village">
                <span>Village traditionnel</span>
            </div>
            <!-- /.img_content -->
            <div data-aos="zoom-in-right" data-aos-duration = "800"  class="img_content collioure">
                <span>Collioure</span> 
            </div>
            <!-- /.img_content -->
        </div>
        <!-- /.content_left -->
        <div class="content_right">
            <div class="main_content">
                <h2 data-aos="fade-left" data-aos-duration = "400">Amazing places to enjoy your travel</h2>
                <p data-aos="fade-top"  data-aos-duration = "400">Etiam facilisis, sapien quis porta dignissim, orci nisi pharetra dui, varius vehicula ligula nulla sit amet lorem. Aenean in vestibulum quam. Cras commodo varius neque, non gravida diam ultrices nec. Cras nulla mauris, fermentum nec libero in</p>
                <a data-aos="fade-bottom" data-aos-delay= "400" data-aos-anchor="#header" href="/" title="lieux à découvrir" data-aos-duration = "400">Explorer</a>
            </div>
             <!-- /.main_content -->
        </div>
        <!-- /.content_right -->
    </section>
    <!-- /#discovery -->

  

    <section id="map">
        <h2 data-aos="fade-left">Carte des logements</h2>
        <div id="map_container">

        </div>
        <!-- /.map_container -->

    </section>
    <!-- /#map -->
</section>
<!-- /#home -->
