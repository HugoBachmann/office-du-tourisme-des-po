<section id="valide_rent">
    <h2>Validation des locations</h2>
    <?php $a = 1 ?>
    <div class="liste mt-4">
        <?php foreach( $rentals as $rental ): ?>
            <div class="col-lg-8 rent_valid">
                <div class="row">
                    <div class="col-1 list_num">
                        <h4><?php echo $a ?></h4>
                    </div>
                    <div class="col-3 border_right">
                        <p>
                           <?php echo $rental->type ?>
                         </p>
                     </div>
                    <!-- /.col-2 -->
                    <div class="col-2 border_right">
                        <p>
                            <span>Id owner: </span>  <?php echo $rental->owner_id ?>
                        </p>
                    </div>
                    <!-- /.col-2 -->
                    <div class="col-3 border_right">
                        <?php 
                         if ($rental->rent_request == 1){
                            echo '<p>En attente de validation</p>';
                        }  else {
                            echo '<p>non</p>';
                        }
                        ?>
                        </p>
                    </div>
                    <!-- /.col-3 -->
                    <div class="col-1">
                        <p>
                            <span>Id: </span>  <?php echo $rental->id ?>
                        </p>
                    </div>
                    <!-- /.col-1 -->
                    <div class="col-1 text-center">
                        <form method="post"action="/admin/validateRent/<?php echo $rental->id ?>">
                        <input type="hidden" name="id" value="<?php echo $rental->id ?>">
                            <button class="btn btn-success"><i class="fas fa-check"></i></button>
                        </form>
                    </div>
                    <!-- /.col-1 -->
                    <div class="col-1 text-center">
                        <form method="post"action="/admin/validateRent/<?php echo $rental->id ?>/delete">
                        <input type="hidden" name="id" value="<?php echo $rental->id ?>">
                            <button class="btn btn-danger"><i class="fas fa-times"></i></button>
                        </form>
                    </div>
                    <!-- /.col-1 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-lg-8 user_valid -->
            <?php $a++?>
        <?php endforeach; ?>
    </div>
    <!-- /.liste -->
</section>
<!-- /#valide_rent -->
