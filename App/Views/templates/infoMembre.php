<section id="info_membre">
    <div class="container">
        <div class="information_user">
            <h3>Informations <span><?php echo $user->firstname ?></span> <span><?php echo $user->lastname ?></span></h3>
            <div class="row">
                <div class="col-6">
                    <p>
                        Prénom: <?php echo $user->firstname ?>
                    </p>
                    <p>
                        Nom: <?php echo $user->lastname ?>
                    </p>
                    <p>
                        id: <?php echo $user->id ?>
                    </p>
                </div>
                <!-- /.col-6 -->
                <div class="col-6">
                    <p>
                    Adresse mail: <?php echo $user->email ?>
                    </p>
                    <p>
                    Hébergeur: <?php 
                    if( $user->is_host){
                    echo 'Oui';
                    } else if ($user->pro_request == 1){
                        echo 'En attente de validation';
                    }  else {
                        echo 'non';
                    }
                    ?>
                    </p>
                    <p>Administrateur: <?php
                        if( $user->is_admin){
                        echo 'Oui';
                        } else{
                            echo 'Non';
                        }  
                        ?>
                    </p>
                </div>
                <!-- /.col-6 -->
            </div>
            <!-- /.row -->
            <div class="form_change">
                <!-- FORM DELETE -->
                <form action="/admin/listeMembre/infoMembre/<?php echo $user->id ?>/delete" method="post" class="delete_form">
                    <input type="hidden" name="id" value="<?php echo $user->id ?>">
                    <?php
                    if ( $user->is_admin){
                        echo '';
                    } else {
                    echo '<input value="Supprimer" type="submit" class="btn btn-danger delete_btn" href="/admin/listeMembre/infoMembre/'. $user->id  .'/delete"></input>';}
                    ?>
                </form>
            </div>
            <!-- /.form_change -->
        </div>
        <!-- /.information_user -->
        <div class="formulaire_modification">
            <h3>Modification de <span><?php echo $user->firstname ?></span> <span><?php echo $user->lastname?></span></h3>
            <form action="/admin/listeMembre/infoMembre/<?php echo $user->id ?>/edit" method="post" novalidate>
                <div class="row">
                    <div class="col-6">
                        <input type="hidden" name="csrf" value="<?php echo $csrf_token ?>">
                        <input placeholder="Prénom" class="form-control" type="text" name="firstname">
                        <input placeholder="Nom" class="form-control" type="text" name="lastname">
                    </div>
                    <!-- /.col-6 -->
                    <div class="col-6">
                        <input placeholder="Email" class="form-control" type="email" name="email">
                        <input placeholder="Mot de passe" class="form-control" type="password" name="password">
                    </div>
                    <!-- /.col-6 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-6">
                        <h4>Type d'utilisateur</h4>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_host" id="tourist_btn" value="0" checked>
                            <label class="form-check-label" for="tourist_btn">
                            Utilisateur
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_host" id="pro_btn" value="1">
                            <label class="form-check-label" for="pro_btn">
                                Professionnel
                            </label>
                        </div>
                    </div>
                    <!-- /.col-6 -->
                    <div class="col-6">
                        <h4>Administrateur</h4>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_admin" id="oui_admin" value="1" checked>
                            <label class="form-check-label" for="oui_admin">
                            Oui
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_admin" id="non_admin" value="0">
                            <label class="form-check-label" for="non_admin">
                                Non
                            </label>
                        </div>
                    </div>
                    <!-- /.col-6 -->
                </div>
                <!-- /.row -->
                <input type="hidden" name="id" value="<?php echo $user->id ?>">
                <input value="Modifier" type="submit" class="btn btn-success modif_btn" href="/admin/listeMembre/infoMembre/'. $user->id  .'/delete"></input>
            </form>    
        </div>
        <!-- /.formulaire_modification -->
    </div>
    <!-- /.container -->
</section>
<!-- /#membre -->