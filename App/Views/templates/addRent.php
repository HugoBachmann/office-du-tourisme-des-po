<section id="add_rent">
    <div class="container">
    <div class="formulaire_ajout">
            <h3>Ajouter une location</h3>
            <form action="/addRent" method="post" novalidate>
                <div class="row">
                    <div class="col-6">
                        <input type="hidden" name="csrf" value="<?php echo $csrf_token ?>">
                        <input type="hidden" name="rent_request" value="1">
                        <input type="hidden" name="rent_validate" value="0">

                        <select  class="form-control" name="type" id="type">
                            <option value="Appartement meublé">Appartement meublé</option>
                            <option value="Gîte">Gîte</option>
                            <option value="Maison à la location">Maison à la location</option>
                            <option value="Mobile home privatif">Mobile home privatif</option>
                        </select>
                        <input placeholder="Adresse" class="form-control" type="text" name="adresse">
                        <input placeholder="Nombre de pièces" class="form-control" type="number" name="nbr_room">
                    </div>
                    <!-- /.col-6 -->
                    <div class="col-6">
                        <input placeholder="Equipement" class="form-control" type="text" name="equipement">
                        <input placeholder="Prix à la nuité (€)" class="form-control" type="number" name="price">
                        <textarea placeholder="Description" class="form-control" name="description"></textarea>
                    </div>
                    <!-- /.col-6 -->
                </div>
                <!-- /.row -->
                <input value="Ajouter" type="submit" class="btn btn-success modif_btn"></input>
            </form>    
        </div>
        <!-- /.formulaire_modification -->
    </div>
    <!-- /.container -->
</section>
<!-- /#add_member -->