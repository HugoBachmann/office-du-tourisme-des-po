<section id="rent_liste">
        <h2>Liste des locations</h2>
        <div class="lists mt-4">
            <div class="row">
                <?php $a = 1 ?>
                <?php foreach( $rentals as $rental ): ?>
                    <div class="rent col-lg-8" id="membre_<?php echo $rental->id ?>">
                        <div class="row">
                            <div class="col-1 list_num">
                                <h4><?php echo $a ?></h4>
                            </div>
                            <!-- /.col-1 -->
                            <div class="col-3 border_right">
                                <p>
                                    <?php echo $rental->type ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-2 border_right">
                                <p>
                                    <span>Id Pro:</span>  <?php echo $rental->owner_id ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-2 border_right">
                                <p>
                                    <?php echo $rental->adresse ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-1 border_right">
                                <p>
                                    <?php echo $rental->price ?> €
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-1">
                                <p>
                                    <span>Id:</span> <?php echo $rental->id ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-2 text-center">
                                <button data-link="http://td-php-objet.lndo.site/admin/listeRent/infoRent/<?php echo $rental->id ?>" class='btn btn-primary btn_single'>Voir</button>
                            </div>
                            <!-- /.col-2 -->
                        </div>
                    </div>
                    <!-- /.rent -->
                    <?php $a++?>
                <?php endforeach; ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.list -->
</section>
<!-- /#membre_liste -->