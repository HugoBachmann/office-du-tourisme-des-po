<section id="footer">
    <div class="container">
        <div class="footer_menu">
            <nav>
                <ul>
                    <li data-aos="fade-left">
                        <a href="/">Accueil</a>
                    </li>
                    <li data-aos="fade-left">
                        <a href="/">Locations</a>
                    </li>
                    <li data-aos="fade-right">
                        <a href="/contact">Contact</a>
                    </li>
                    <li data-aos="fade-right">
                        <?php if( empty( $_SESSION )){
                            echo "<a class='login_btn' title='Se connecter' href='/login'>Se connecter/S'incrire</a>";
                        } else {
                            echo "<a class='logout_btn' title='Se déconnecter' href='/logout'>Se déconnecter</a>";
                        }?>     
                    </li>
                </ul>
            </nav>
        </div>
        <!-- /.footer_menu -->
        <!-- <div class="copyright">
            <p><span>Conception</span> : Hugo Bachmann</p>
        </div> -->
        <!-- /.copyright -->
        <div class="socials">
            <nav>
                <ul>
                    <li data-aos="fade-right" data-aos-anchor="#footer" data-aos-duration = "700">
                        <a class="facebook" title="Facebook PO" target="_blank" href="https://fr-fr.facebook.com/tourisme.pyreneesorientales/"><i class="fab fa-facebook-square"></i></a>
                    </li>
                    <li data-aos="fade-up" data-aos-anchor="#footer" data-aos-duration = "700">
                        <a class="instagram" target="_blank" title="Instagram PO" href="https://www.instagram.com/pyreneesorientales_tourisme/?hl=fr"><i class="fab fa-instagram-square"></i></a>
                    </li>
                    <li data-aos="fade-left" data-aos-anchor="#footer" data-aos-duration = "700">
                        <a class="twitter" target="_blank" title="Twitter PO" href="https://twitter.com/ledepartement66?lang=fr"><i class="fab fa-twitter-square"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- /.socials -->
    </div>
    <!-- /.container -->
</section>
<!-- /#footer -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="<?php echo(JS_PATH . "script.js"); ?>"> </script>
<script src="<?php echo(JS_PATH . "tilt.jquery.min.js"); ?>"> </script>
<script src='https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.js'></script>
<script src="https://unpkg.com/es6-promise@4.2.4/dist/es6-promise.auto.min.js"></script>
<script src="https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js"></script>
<script src="<?php echo(JS_PATH . "map.js"); ?>"> </script>
</body>
</html>