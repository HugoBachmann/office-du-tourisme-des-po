<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Exo:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="<?php echo(CSS_PATH . "style.css"); ?>">
    
    <title>Office du tourisme</title>
</head>
<body>
        <header id="header" class="d-flex justify-content-between">
            <a href="/"><h1>Les pyrénées orientales</h1></a>
            <div class="menu_burger">
                <i class="fas fa-bars"></i>
            </div>
            <!-- /.menu_burger -->
             <div class="mobile_menu">
                <a class="home_btn" title="Accueil" href="/"><i class="fas fa-home"></i></a>
                <a class="rent_btn" title="Les locations" href="/locations"><i class="fas fa-bed"></i></a>
                <a class="contact_btn" title="Contact" href="/contact"><i class="fas fa-envelope"></i></a>
                <?php
                use App\Session;
                $utilisateur = Session::get(Session::SESSION_USER);
                $is_connect = !is_null($utilisateur);
                $is_admin = $is_connect && $utilisateur->is_admin;
                if( $is_connect ){
                    if( $is_admin ) {
                        echo '<a class="admin_btn" title="Admin" href="/admin"><i class="fas fa-cog"></i></a>';
                    }
                }
                ?>
                <?php 
                $utilisateur = Session::get(Session::SESSION_USER);
                $is_connect = !is_null($utilisateur);
                $is_host = $is_connect && $utilisateur->is_host;
                if( $is_connect ){
                    if( $is_host ) {
                        echo '<a class="admin_host_btn" title="Admin" href="/adminhost/'. $utilisateur->id .'"><i class="fas fa-building"></i></a>';
                    }
                }
                ?>
                <?php if( empty( $_SESSION )){
                    echo "<a class='login_btn' title='Se connecter' href='/login'><i class='far fa-user'></i></a>";
                    } else {
                        echo "<a class='logout_btn' title='Se déconnecter' href='/logout'><i class='fas fa-sign-out-alt'></i></i></a>";
                    }?>     
             </div>
             <!-- /.mobile_menu -->
        </header>


    
