<section id="register">
        <?php if( ! is_null( $form_status ) ): ?>
            div><?php echo $form_status->message ?></div>
        <?php endif; ?>
    <div class="content_register">


        <div  data-aos="flip-left" data-aos-duration = "900" class="left_content">
            <div data-tilt class="img_register_container">
                <img src="<?php echo(IMG_PATH . "imgLogin.png"); ?>" alt="S'inscrire">
            </div>
            <!-- /.img_register_container -->
        </div>
        <!-- /.left_content-->


        <div class="right_content"> 
            <div class="content_form">
                <h3 data-aos="fade-left" data-aos-anchor="#header">S'inscrire</h3>
                <form  data-aos="fade-left" data-aos-anchor="#header"  data-aos-delay= "350" class="form-group" action="/reg" method="post">
                    <input type="hidden" name="csrf" value="<?php echo $csrf_token ?>">
                    <input type="hidden" name="is_admin" value="0">
                    <input class="form-control" type="text" name="firstname" placeholder="Prénom">
                    <input class="form-control" type="text" name="lastname" placeholder="Nom">
                    <input class="form-control" type="email" name="email" placeholder="Email">
                    <input class="form-control" type="password" name="password" placeholder="Mot de passe">
                    <div class="radio_btn">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="pro_request" id="tourist_btn" value="1" checked>
                            <label class="form-check-label" for="tourist_btn">
                               Aventurier
                            </label>
                        </div>
                        <!-- form-check -->
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="pro_request" id="pro_btn" value="0">
                            <label class="form-check-label" for="pro_btn">
                                Professionnel
                            </label>
                        </div>
                        <!-- form-check -->
                    </div>
                    <!-- /.radio_btn -->
                    <input  class="btn_sumbit btn btn-success" type="submit" value="S'inscrire">
                </form>
                <div  data-aos="fade-up" data-aos-anchor="#header" data-aos-delay= "800" class="deja_inscrit">
                    <a title="Se connecter" href="/login" >Connecter-vous <i class="fas fa-arrow-right"></i></a>
                </div>
                <!-- /.deja_inscrit -->
            </div>
            <!-- /.content_form -->
        </div>
         <!-- /.right_content -->


    </div>
    <!-- /.content_register -->
</section>
<!-- /#register -->
