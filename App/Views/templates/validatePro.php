<section id="valide_pro">  
    <h2>Validation compte Pro</h2>
    <?php $a = 1 ?>
        <div class="liste mt-4">
        <?php foreach( $users as $user ): ?>
            <div class="col-lg-8 user_pro">
                <div class="row">
                    <div class="col-1 list_num">
                        <h4><?php echo $a ?></h4>
                    </div>
                    <div class="col-2 border_right">
                        <p>
                            <span>Prénom: </span>  <?php echo $user->firstname ?>
                         </p>
                     </div>
                    <!-- /.col-2 -->
                    <div class="col-2 border_right">
                        <p>
                            <span>Nom:</span>  <?php echo $user->lastname ?>
                        </p>
                    </div>
                    <!-- /.col-2 -->
                    <div class="col-4 border_right">
                        <?php 
                        if( $user->is_host){
                        echo '<p>Oui</p>';
                        } else if ($user->pro_request == 1){
                            echo '<p>En attente de validation</p>';
                        }  else {
                            echo '<p>non</p>';
                        }
                        ?>
                        </p>
                    </div>
                    <!-- /.col-2 -->
                    <div class="col-1">
                        <p>
                            </span>id: </span>  <?php echo $user->id ?>
                        </p>
                    </div>
                    <!-- /.col-1 -->
                    <div class="col-1 text-center">
                        <form method="post"action="/admin/validatePro/<?php echo $user->id ?>">
                        <input type="hidden" name="id" value="<?php echo $user->id ?>">
                            <button class="btn btn-success"><i class="fas fa-check"></i></button>
                        </form>
                    </div>
                    <!-- /.col-2 -->
                    <div class="col-1 text-center">
                        <form method="post" action="/admin/validatePro/<?php echo $user->id ?>/denied">
                        <input type="hidden" name="id" value="<?php echo $user->id ?>">
                            <button class="btn btn-danger"><i class="fas fa-times"></i></button>
                        </form>
                    </div>
                    <!-- /.col-2 -->
                </div>
                <!-- /.row -->    
            </div>
            <!-- /.validation_content -->
            <?php $a++?>
        <?php endforeach; ?>
        </div>
        <!-- /.liste -->
</section>
<!-- /#valide_pro -->
