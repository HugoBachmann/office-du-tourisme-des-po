<section id="list_location_front">
    <div class="container">
        <h2 data-aos="fade-right">Découvrez nos locations </h2>
        <div class="row content">
        <?php foreach( $rentals as $rental ): ?>
            <div class="card col-4" data-aos="fade-up">
                <div class="img_container">
                    <img src="<?php echo(IMG_PATH . "iconLocation.png"); ?>" class="" alt="Location PO">
                </div>
                <!-- /.img_container -->
                <div class="card-body">
                    <h5 class="card-title"><?php echo $rental->type ?></h5>
                    <p class="card-text"><?php echo $rental->description ?></p>
                    <a href="/location/detailLocation/<?php echo $rental->id ?>" class="btn btn-primary">Voir</a>
                </div>
            </div>
            <!-- /.card -->
        <?php endforeach; ?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /#listLocationFront -->
