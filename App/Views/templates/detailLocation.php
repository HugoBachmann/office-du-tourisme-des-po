<section id="detail_location">
    <div class="container">
        <div class="content" data-aos="fade-up">
            <h2>Location:  <?php echo $rentals->adresse ?></h2>
            <div  data-tilt class="img_content">
                <img src="<?php echo(IMG_PATH . "iconLocation.png"); ?>" alt="location PO">
            </div>
            <!-- /.img_content -->
            <div class="row">
                <div class="col-6">
                    <p>
                        <strong>Type: </strong> <?php echo $rentals->type ?>
                    </p>
                    <p>
                        <strong>Adresse: </strong> <?php echo $rentals->adresse ?>
                    </p>
                    <p>
                        <strong><?php echo $rentals->price  ?></strong> €/nuit
                    </p>
                </div>
                <!-- /.col-6 -->
                <div class="col-6">
                    <p>
                        <strong>Type: </strong> <?php echo $rentals->equipement ?>
                    </p>
                    <p>
                        <strong>Adresse: </strong> <?php echo $rentals->description ?>
                    </p>
                    <p>
                        <strong>Nombre de pièces: </strong> <?php echo $rentals->nbr_room ?>
                    </p>
                </div>
                <!-- /.col-6 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <a href="/" class="btn btn-success">Contacter le propriétaire</a>
            </div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.container -->
</section>
<!-- /#detail_location -->

