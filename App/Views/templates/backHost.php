<section id="back_pro">
    <div class="container">
        <h2>Gestion de mes annonces</h2>
        <div class="content_pro">
            <?php
            use App\Session;

            $user = Session::get( Session::SESSION_USER );
                    $user_id = $user->id;
            ?>
            <?php foreach($rentals as $rental):?>
                <?php if ( $rental->owner_id == $user_id) :?>
                <div class="unit_rent">
                    <h3>Location <?php echo $rental->adresse ?></h3>
                    <div class="row ">
                        <div class="col-8">
                            <div class="row">
                                    <div class="col-4 top">
                                        <strong>Type:</strong> <?php echo $rental->type ?>
                                    </div>
                                    <!-- /.col-4 -->
                                    <div class="col-4 top">
                                        <strong>Adresse:</strong> <?php echo $rental->adresse ?>
                                    </div>
                                    <!-- /.col-4 -->
                                    <div class="col-2 top">
                                        <strong>Tarif:</strong> <?php echo $rental->price ?> €
                                    </div>
                                    <!-- /.col-1 -->
                                    <div class="col-2 text-center">
                                        <a href="http://td-php-objet.lndo.site/backhost/rent/<?php echo $rental->id ?>" class='btn btn-primary btn_single'>Voir</a>
                                    </div>
                            <!-- /.col-2 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.col-8 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- unit_rent -->  
                <?php endif ?>
            <?php endforeach; ?>
        </div>
        <!-- /.content_pro -->
    </div>
    <!-- /.container -->
</section>
