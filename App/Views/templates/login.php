 
 <section id="login" class="justify-content-center">
    <div class="content_login">


        <div class="left_content">
            <div class="content_form">
                <h3 data-aos="fade-right" data-aos-anchor="#header">Connexion</h3>
                <?php if( ! is_null( $form_status ) ): ?>
                    <div><?php echo $form_status->message ?></div>
                <?php endif; ?>
                <form data-aos="fade-right" data-aos-anchor="#header"  data-aos-delay= "350" class="form-group" action="/auth" method="post" novalidate>
                    <input class="form-control" type="hidden" name="csrf" value="<?php echo $csrf_token ?>">
                    <input class="form-control" type="email" name="email" placeholder="Email">
                    <input class="form-control" type="password" name="password" placeholder="Mot de passe">
                    <input class="btn btn-success" type="submit" value="Se connecter">
                </form>    
                <div data-aos="fade-up" data-aos-anchor="#header" data-aos-delay= "800" class="non_register">
                    <a href="/register">Créez votre compte <i class="fas fa-arrow-right"></i></a>
                </div>
                <!-- /.non_register -->
            </div>
            <!-- /.content_form -->
        </div>
        <!-- /.left_content -->


        <div data-aos="flip-right" data-aos-duration = "900" class="right_content">
            <div data-tilt class="img_register_container">
                <img src="<?php echo(IMG_PATH . "imgLogin.png"); ?>" alt="Se connecter">
            </div>
            <!-- /.img_register_container -->
        </div>
        <!-- /.right_content -->


    </div>
     <!-- /.content_login -->
</section>
 <!-- /.login -->
