<section id="rent">
    <div class="container">
    <div class="information_rent">
            <h3>Détails de la location n°<?php echo $rentals->id ?></h3>
            <div class="row">
                <div class="col-6">
                    <p>
                        Type de bien <?php echo $rentals->type ?>
                    </p>
                    <p>
                        Adresse <?php echo $rentals->adresse ?>
                    </p>
                    <p>
                        Prix de la nuité <?php echo $rentals->price ?>
                    </p>
                    <p>
                        Nombre de pièces: <?php echo $rentals->nbr_room ?>
                    </p>
                </div>
                <!-- /.col-6 -->
                <div class="col-6">
                    <p>
                         ID de propriétaire <?php echo $rentals->owner_id ?>
                    </p>
                    <p>Status: 
                        <?php 
                            if ($rentals->rent_validate == 1){
                                echo "Annonce Validé";
                            } else {
                                echo "En attente de validation";
                            }
                         
                        ?>
                    </p>
                    <p>
                            Equipement: <?php echo $rentals->equipement ?>
                    </p>
                    <p>
                            Description: <?php echo $rentals->description ?>
                    </p>
                </div>
                <!-- /.col-6 -->
            </div>
            <!-- /.row -->
            <div class="form_change">
                <!-- FORM DELETE -->
                <form action="/adminhost/rent/<?php echo $rentals->id ?>/delete" method="post" class="delete_form">
                    <input type="hidden" name="id" value="<?php echo $rentals->id ?>">
                    <input value="Supprimer" type="submit" class="btn btn-danger delete_btn" href="/admin/listeMembre/infoMembre/'. $user->id  .'/delete"></input>
                </form>
            </div>
            <!-- /.form_change -->
        </div>
        <!-- /.information_rent -->
        <div class="formulaire_modification">
            <h3>Modifier</h3>
            <form action="/adminhost/rent/<?php echo $rentals->id?>/edit" method="post" novalidate>
                <div class="row">
                    <div class="col-6">
                        <input type="hidden" name="id" value="<?php echo intval($rentals->id) ?>">
                        <input type="hidden" name="owner_id" value="<?php echo intval($rentals->owner_id) ?>">
                        <input type="hidden" name="rent_request" value="<?php echo $rentals->rent_request ?>">
                        <input type="hidden" name="rent_validate" value="<?php echo $rentals->rent_validate ?>">
                        <select name="type" id="type" class="form-control">
                            <option value="Appartement meublé">Appartement meublé</option>
                            <option value="Gîte">Gîte</option>
                            <option value="Maison à la location">Maison à la location</option>
                            <option value="Mobile home privatif">Mobile home privatif</option>
                        </select>
                        <input placeholder="Adresse" class="form-control" type="text" name="adresse">
                        <input placeholder="Nombre de pièces" class="form-control" type="number" name="nbr_room">
                    </div>
                    <!-- /.col-6 -->  
                    <div class="col-6">
                        <input placeholder="Equipement" class="form-control" type="text" name="equipement">
                        <input placeholder="Prix à la nuité (€)" class="form-control" type="number" name="price">
                        <textarea placeholder="Description" class="form-control" name="description"></textarea>
                    </div>
                    <!-- /.col-6 -->    
                </div>
                <!-- /.row -->
                <input value="Modifier" type="submit" class="btn btn-success modif_btn"></input>
            </form>        
        </div>
        <!-- /.formulaire_modification -->
    </div>
    <!-- /.container -->

</section>
<!-- /#rent -->