<section id="add_member">
    <div class="container">
    <div class="formulaire_ajout">
            <h3>Ajouter un utilisateur</h3>
            <form action="/admin/addMember" method="post" novalidate>
                <div class="row">
                    <div class="col-6">
                        <input type="hidden" name="csrf" value="<?php echo $csrf_token ?>">
                        <input placeholder="Prénom" class="form-control" type="text" name="firstname">
                        <input placeholder="Nom" class="form-control" type="text" name="lastname">
                    </div>
                    <!-- /.col-6 -->
                    <div class="col-6">
                        <input placeholder="Email" class="form-control" type="email" name="email">
                        <input placeholder="Mot de passe" class="form-control" type="password" name="password">
                    </div>
                    <!-- /.col-6 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-6">
                        <h4>Type d'utilisateur</h4>
                        <div class="form-check">
                            <input type="hidden" name="is_host" value="0">
                            <input class="form-check-input" type="radio" name="pro_request" id="tourist_btn" value="0" checked>
                            <label class="form-check-label" for="tourist_btn">
                            Utilisateur
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="pro_request" id="pro_btn" value="1">
                            <label class="form-check-label" for="pro_btn">
                                Professionnel
                            </label>
                        </div>
                    </div>
                    <!-- /.col-6 -->
                    <div class="col-6">
                        <h4>Administrateur</h4>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_admin" id="oui_admin" value="1" checked>
                            <label class="form-check-label" for="oui_admin">
                                Oui
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_admin" id="non_admin" value="0">
                            <label class="form-check-label" for="non_admin">
                                Non
                            </label>
                        </div>
                    </div>
                    <!-- /.col-6 -->
                </div>
                <!-- /.row -->
                <input value="Ajouter" type="submit" class="btn btn-success modif_btn"></input>
            </form>    
        </div>
        <!-- /.formulaire_modification -->
    </div>
    <!-- /.container -->
</section>
<!-- /#add_member -->