<section id="membre_liste">
        <h2>Liste des membres</h2>
        <div class="lists mt-4">
            <div class="row">
                <?php $a = 1 ?>
                <?php foreach( $user as $personne ): ?>
                    <div class="user col-lg-8" id="membre_<?php echo $personne->id ?>">
                        <div class="row">
                            <div class="col-1 list_num">
                                <h4><?php echo $a ?></h4>
                            </div>
                            <!-- /.col-1 -->
                            <div class="col-4 border_right">
                                <p>
                                    <span>Nom:</span>  <?php echo $personne->lastname ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-4 border_right">
                                <p>
                                    <?php echo $personne->email ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-1">
                                <p>
                                    <span>Id:</span> <?php echo $personne->id ?>
                                </p>
                            </div>
                            <!-- /.col-2 -->
                            <div class="col-2 text-center">
                                <button data-link="http://td-php-objet.lndo.site/admin/listeMembre/infoMembre/<?php echo $personne->id ?>" class='btn btn-primary btn_individual'>Voir</button>
                            </div>
                            <!-- /.col-2 -->
                        </div>
                    </div>
                    <!-- /.user -->
                    <?php $a++?>
                <?php endforeach; ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.list -->
</section>
<!-- /#membre_liste -->