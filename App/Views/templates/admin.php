
<section id="backoffice">
    <div class="admin_menu col-2">
        <a class="administration"><i class="fas fa-unlock"></i> Administration</a>
        
        <div class="gestion">
            <a class="btn_drop_admin btn_gestion sub_cat"><i class="fas fa-cogs"></i> Gestion <i class="fas fa-caret-down dropdown_gestion"></i></a>
            <div class="content content_admin_menu_gestion">
                <a class="low_cat cat_member"><i class="fas fa-users"></i> Gestion des membres</a>
                <a class="low_cat add_member"><i class="fas fa-user-plus"></i> Ajout d'utilisateurs</a>
                <a class="low_cat cat_rent"><i class="fas fa-bed"></i> Gestion des locations</a>
                <a class="low_cat add_rent"><i class="fas fa-folder-plus"></i> Ajout de locations</a>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.gestion -->

        <div class="validation">
            <a class="btn_drop_admin btn_validation sub_cat"><i class="fas fa-check"></i> Validation <i class="fas fa-caret-down dropdown_validation"></i></a>
            .<div class="content content_admin_menu_validation">
                <a class="low_cat validation_post"><i class="fas fa-clipboard-check"></i> Validations des annonces</a>
                <a class="low_cat pro_validation"><i class="fas fa-user-check"></i>Validations des Pro</a>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.validation -->

        <div class="contact">
            <a class="btn_drop_admin btn_contact sub_cat"><i class="fas fa-envelope"></i> Contact <i class="fas fa-caret-down dropdown_contact"></i></a>
            .<div class="content content_admin_menu_contact">
                <a class="low_cat contact_form" ><i class="fas fa-envelope-open"></i> Demande de contact</a>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.contanct -->
    </div>
    <!-- /.admin_menu -->
    <div class="admin_content col-10">

    </div>
    <!-- /.admin_content -->
</section>
<!-- /#backoffice -->