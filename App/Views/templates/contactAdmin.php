<section id="msg_admin">
    <h2>Demande de contact</h2>
        <div class="liste">
            <div class="row">
                <div class="col-7">
                <?php foreach( $contacts as $contact ): ?>
                    <div class="msg">
                        <div class="row">
                            <div class="col-3">
                                <p class="nom">
                                    <?php echo $contact->user_name ?>
                                </p>
                            </div>
                            <!-- /.col-3 -->
                            <div class="col-3">
                                <p class="email">
                                    <?php echo $contact->user_email ?>
                                </p>
                            </div>
                            <!-- /.col-3 -->
                            <div class="col-2 offset-1">
                                <a class="btn btn-success" href="mailto:<?php echo $contact->user_email ?>">Répondre</a>
                            </div>
                            <!-- /.col-3 -->
                            <div class="col-2">
                                <form action="admin/contactAdmin/<?php echo $contact->id ?>/Delete" method="post">
                                    <input type="hidden" value="<?php echo $contact->id ?>" name="id">
                                    <input type="submit" class="btn btn-danger" value="Supprimer"></input>
                                </form>
                            </div>
                            <!-- /.col-3 -->
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-12 paragraphe">
                                <p>
                                    <?php echo $contact->user_msg ?>
                                </p>
                            </div>
                            <!-- /.col-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.msg -->
                <?php endforeach; ?>
                </div>
                <!-- /.col-7 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.liste -->
</section>
<!-- /#msg_admin -->
