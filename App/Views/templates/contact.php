<section id="contact">
    <div class="content_contact">


        <div data-aos="flip-left" data-aos-duration = "900" class="left_content">
            <div data-tilt class="img_register_container">
                <img src="<?php echo(IMG_PATH . "imgContact.png"); ?>" alt="Contact">
            </div>
            <!-- /.img_register_container -->
        </div>
        <!-- /.left_content -->


        <div class="right_content">
            <div class="content_form">
                <h3 data-aos="fade-left" data-aos-anchor="#header">Contactez-nous</h3>
                <form data-aos="fade-left" data-aos-anchor="#header"  data-aos-delay= "350" class="form-group" action="/contactPost" method="post" novalidate>
                    <input type="hidden" name="csrf" value="<?php echo $csrf_token ?>">
                    <input class="form-control" type="text" name="user_name" placeholder="Nom">
                    <input class="form-control" type="email" name="user_email" placeholder="Email">
                    <textarea class="form-control" placeholder="Message" name="user_msg"></textarea>
                    <button class="btn_sumbit btn btn-success" type="submit" >Envoyer le message <i class="fas fa-arrow-right"></i></button>
                </form>
            </div>
            <!-- /.content_form -->
        </div>
        <!-- /.right_content -->


    </div>
    <!-- /.content_contact -->
</section>
<!-- /#contact -->

   