<?php

namespace App;

use Closure;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;
use App\Views\View;
use App\Forms\AntiCsrf;
use App\Forms\FormStatus;

class AdminMiddleware
{   

    public function handle( ServerRequestInterface $request, Closure $next )
    {
        $user = Session::get( Session::SESSION_USER );
        $is_auth = ! is_null( $user );
        $is_admin = $is_auth && $user->is_admin;

        if( $is_admin ) {
            return $next( $request );
        }
        return header( 'Location: /' );
    }
}
