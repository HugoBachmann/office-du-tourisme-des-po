<?php

namespace App;

use MiladRahimi\PhpRouter\Router;
use App\Controllers\AuthController;
use App\Controllers\AdminController;
use App\Controllers\ContactController;
use App\Controllers\RentalController;
use App\Controllers\PageController;
use App\Controllers\UserController;
use App\AdminMiddleware;
use App\Repositories\UserRepository;

class App
{
    private static ?self $instance = null;

    private ?Router $router = null;
    public function getRouter(): ?Router { return $this->router; }
    
    public static function getInstance(): self
    {
        if( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function start(): void
    {
        session_start();

        $app = self::getInstance();

        $app->initRouter();
        Database::getInstance()->getPdo();
    }

    private function initRouter(): void
    {
        $this->router = Router::create();

        $this->initRoutes();
        $this->router->dispatch();
    }

    private function initRoutes():void
    {
        $this->router->get( '/', [ PageController::class, 'index' ] );
        $this->router->get( '/getmapmarkers', [ PageController::class, 'returnMapMarkers' ] );

        // PAGE DE LOGIN
        $this->router->get( '/login', [ AuthController::class, 'login' ] );
        $this->router->post( '/auth', [ AuthController::class, 'auth' ] );
        $this->router->get( '/logout', [ AuthController::class, 'logout' ] );

        // PAGE DE CONTACT
        $this->router->get( '/contact', [ ContactController::class, 'contactFront' ] );
        $this->router->post( '/contactPost', [ ContactController::class, 'contactPost' ] );

        // PAGE DE REGISTER
        $this->router->get( '/register', [ AuthController::class, 'register' ] );
        $this->router->post( '/reg', [ AuthController::class, 'reg' ] );

        //PAGE DE LOCATION
        $this->router->get( '/locations', [ RentalController::class, 'displayRent' ] );
        $this->router->get( '/location/detailLocation/{id}', [ RentalController::class, 'displayRentId' ] );

        // Page pour les admins
        $this->router->group(
            [
                'middleware' => [ AdminMiddleware::class ]
            ],
            function( Router $router ) {
                
                // ADMIN PAGE
                $router->get( '/admin', [ PageController::class, 'admin' ] );

                // VALIDER PRO
                $router->get( '/admin/validatePro', [ AdminController::class, 'proRequest' ] );
                $router->post( '/admin/validatePro/{id}', [ AdminController::class, 'validatePro' ] );
                $router->post( '/admin/validatePro/{id}/denied', [ AdminController::class, 'deniedPro' ] );
                
                // ADD MEMBER
                $router->get( '/admin/addMember', [ AuthController::class, 'addUser' ] );
                $router->post( '/admin/addMember', [ AuthController::class, 'addUserPost' ] );
                
                // ADD RENT
                $router->get( '/addRent', [ RentalController::class, 'addRentals' ] );
                $router->post( '/addRent', [ RentalController::class, 'addRentalsPost'] );

                // DEFAULT ADMIN CONTENT
                $router->get( '/admin/infoAdmin', [ PageController::class, 'infoAdmin' ] );
                
                // LISTE MEMBRE
                $router->get( '/admin/listeMembre', [ UserController::class, 'listMembre' ] );
                $router->post( '/admin/listeMembre/infoMembre/{id}/delete', [ AdminController::class, 'deleteUser' ] );
                $router->post( '/admin/listeMembre/infoMembre/{id}/edit', [ AdminController::class, 'updateUser' ] );
                $router->get( '/admin/listeMembre/infoMembre/{id}', [ UserController::class, 'infoMembre' ] );

                // LISTE DES RENT
                $router->get( '/admin/listeRent', [ RentalController::class, 'listeRent' ] );
                $router->get( '/admin/listeRent/infoRent/{id}', [ RentalController::class, 'infoRent' ] );
                $router->post( '/admin/listeRent/infoRent/{id}/delete', [ RentalController::class, 'deleteRent' ] );
                $router->post( '/admin/listeRent/infoRent/{id}/edit', [ RentalController::class, 'updateRent' ] );

                // VALIDATE RENT
                $router->get( '/admin/validateRent', [ RentalController::class, 'rentalRequest' ] );
                $router->post( '/admin/validateRent/{id}', [ RentalController::class, 'validateRent' ] );
                $router->post( '/admin/validateRent/{id}/delete', [ RentalController::class, 'deleteRent' ] );

                // CONTACT ADMIN 
                $router->get( '/admin/contactAdmin', [ ContactController::class, 'contactList' ] );
                $router->post( '/admin/contactAdmin/{id}/Delete', [ AdminController::class, 'contactDelete' ] );
            });


        // Page pour les herbergeurs
        $this->router->group(
            [
                'Promiddleware' => [ ProMiddleware::class ]
            ],
            function( Router $router ) {
                // BACKOFFICE HOST
                $router->get( '/adminhost/{id}', [ PageController::class, 'backHost' ] );

                 // AJOUT DE LOCATIONS
                $router->get( '/addRent', [ RentalController::class, 'addRentals' ] );
                $router->post( '/addRent', [ RentalController::class, 'addRentalsPost'] );

                // DELETE LOCATION
                $router->post( '/adminhost/rent/{id}/delete', [ RentalController::class, 'deleteRent' ] );

                // UNIQUE RENT 
                $router->get( '/backhost/rent/{id}', [ RentalController::class, 'rent' ] );
                
                // EDIT RENT
                $router->post( '/adminhost/rent/{id}/edit', [ RentalController::class, 'updateRent' ] );
            });
    }

    private function __construct() { }

    private function __clone() { }

    private function __wakeup() { }
}