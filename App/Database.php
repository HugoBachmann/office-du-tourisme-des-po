<?php

namespace App;

use PDO;
use PDOException;

class Database
{
    private static ?self $instance = null;

    private ?PDO $pdo = null;
    public function getPdo(): ?PDO
    {
        if( is_null( $this->pdo ) ) {
            // mysql:dbname=testdb;host=127.0.0.1
            $dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST;

            // Options PDO
            $pdo_options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ];

            try
            {
                $this->pdo = new PDO( $dsn, DB_USER, DB_PASS, $pdo_options );
                // echo "Connection à la BDD réussie";
            }
            catch( PDOException $pdo_e )
            {
                // echo "Connection à la BDD échoué";
                return null;
            }
        }
        
        return $this->pdo;
    }

    public static function getInstance(): self
    {
        if( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() { }

    private function __clone() { }

    private function __wakeup() { }
}
