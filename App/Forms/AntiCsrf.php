<?php 
namespace App\Forms;

use DateTime;

use App\Session;

class AntiCsrf
{
    public static function generateToken( string $additional_data = ''): string
    {
        // Création du token et enregitrement en session
        // hash( 'sha512', SALT . session_id . Timestamp . $additional_data . PEPPER )
        $timestamp = ( new DateTime() )->getTimestamp();
        $token = hash( 'sha512', SALT . session_id() . $timestamp . $additional_data . PEPPER );

        Session::set( Session::SESSION_ANTI_CSRF_TOKEN, $token );

        return $token;
    }

    public static function checkToken( string $token ): bool
    {
        return $token === Session::get( Session::SESSION_ANTI_CSRF_TOKEN );
    }
}