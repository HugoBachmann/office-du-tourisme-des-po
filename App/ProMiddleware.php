<?php 

namespace App;

use Closure;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;
use App\Views\View;
use App\Forms\AntiCsrf;
use App\Forms\FormStatus;

class ProMiddleware
{
    public function handlePro( ServerRequestInterface $request, Closure $next )
    {
        $user = Session::get( Session::SESSION_USER );
        $is_auth = ! is_null( $user );
        $is_host = $is_auth && $user->is_host;

        if( $is_host ) {
            return $next( $request );
        }
        
        return header( 'Location: /' );
    }
}