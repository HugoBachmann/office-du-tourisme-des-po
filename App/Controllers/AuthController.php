<?php

namespace App\Controllers;

use Laminas\Diactoros\ServerRequest;

use App\Forms\AntiCsrf;
use App\Forms\FormStatus;
use App\Repositories\UserRepository;
use App\RepositoryManager;
use App\Session;
use App\Views\View;

class AuthController extends Controller
{
    // Login: GET
    public function login(): void
    {
        $view = new View( 'login' );
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get( Session::SESSION_FORM_STATUS )
        ];

        Session::set( Session::SESSION_FORM_STATUS, null );

        $view->render( $data );
    }
    
    // Login: POST
    public function auth( ServerRequest $request ): void
    {
        $post_data = $request->getParsedBody();

        $this->csrfGuard( $post_data['csrf'] );

        $user = RepositoryManager::getRm()
            ->getUserRepo()
            ->auth( $post_data[ 'email' ], $post_data[ 'password' ] );

        if( ! is_null( $user ) ) {
            Session::set( Session::SESSION_USER, $user );

            header( 'Location: /' );
            die();
        } else{
            $status = new FormStatus();
            $status->success = false ;
            $status->message = 'Email ou mot de passe invalides !';
    
            Session::set( Session::SESSION_FORM_STATUS, $status );
    
            header( 'Location: /login' );
        }
    }

      // REGISTER GET
    public function register( ): void
    {
        $view = new View( 'register' );
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get( Session::SESSION_FORM_STATUS )
        ];

        Session::set( Session::SESSION_FORM_STATUS, null );

        $view->render( $data );
    }
        // REGISTER POST
    public function reg ( ServerRequest $request ) :void
    {
        $post_data = $request->getParsedBody();
        // var_dump($post_data);
        $this->csrfGuard( $post_data['csrf'] );

        $user = RepositoryManager::getRm()
            ->getUserRepo()
            ->reg(  $post_data[ 'firstname' ],  $post_data[ 'lastname' ], $post_data[ 'email' ], $post_data[ 'password' ], $post_data[ 'is_admin' ], $post_data[ 'is_host' ] );

        if( !empty( $_SESSION ) ) {
            Session::set( Session::SESSION_USER, $user );

            header( 'Location: /' );
            die();
        } else {
            $status = new FormStatus();
            $status->success = false ;
            $status->message = 'Remplissez tous les champs';
    
            Session::set( Session::SESSION_FORM_STATUS, $status );
    
            header( 'Location: /register' );
        }
    }

        // ADD USER GET
    public function addUser ():void
    {
        $view = new View( 'addMember' );
        $view->template_only = true;
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get( Session::SESSION_FORM_STATUS )
        ];

        $view->render( $data );
    }

        // ADD USER POST
    public function addUserPost ( ServerRequest $request ):void
    {
        $post_data = $request->getParsedBody();
        // var_dump($post_data);
        $this->csrfGuard( $post_data['csrf'] );

        if(!RepositoryManager::getRm()->getUserRepo()->reg($post_data['firstname'], $post_data['lastname'], $post_data['email'], $post_data['password'], $post_data['is_admin'], $post_data[ 'is_host' ])) {
            // header('Location: /admin');
            echo 'erreur de register';
        } else {
            echo 'register send';
            // header('Location: /admin');
        }
    }

    // Logout: GET
    public function logout(): void
    {
        unset( $_SESSION );
        session_destroy();

        header( 'Location: /' );
    }
}
