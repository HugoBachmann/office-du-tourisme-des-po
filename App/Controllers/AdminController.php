<?php 

namespace App\Controllers;

use Laminas\Diactoros\ServerRequest;
use App\Views\View;
use App\RepositoryManager;
use App\Repositories\UserRepository;
use App\Session;
use App\Forms\AntiCsrf;

class AdminController extends Controller
{
    // Delete User: POST
    public function deleteUser( ServerRequest $request ): void
    {

        $post_data = $request->getParsedBody();

        $user = RepositoryManager::getRm()
        ->getUserRepo()
        ->deleteUser( $post_data[ 'id' ]);

        if ( !is_null($user) ) {
            echo 'Une erreur est survenu';
            
        } else {
            header( 'Location: /admin' );
            die();
        }
    }

     // Delete Contact: POST
     public function contactDelete( ServerRequest $request ): void
     {
 
         $post_data = $request->getParsedBody();
 
         $contact = RepositoryManager::getRm()
         ->getContactRepo()
         ->contactDelete( $post_data[ 'id' ]);
 
         if ( !is_null($contact) ) {
             echo 'Une erreur est survenu';
             
         } else {
             header( 'Location: /admin' );
             die();
         }
     }

    // Update: User 
    public function updateUser( ServerRequest $request):void
    {
        $post_data = $request->getParsedBody();
        
        $user = RepositoryManager::getRm()

        ->getUserRepo()
        ->updateUser($post_data[ 'id' ],  $post_data[ 'firstname' ],  $post_data[ 'lastname' ], $post_data[ 'email' ], $post_data[ 'password' ], $post_data[ 'is_admin' ], $post_data[ 'is_host' ] );
        
        if ( !is_null($user) ) {
            echo 'Une erreur est survenu';
            
        } else {
            header( 'Location: /admin' );
            die();
        }
    }

    // Liste non-validateUser
    public function proRequest ( ):void
    {
        $view = new View("validatePro");
        $view->template_only = true;

        $users = RepositoryManager::getRm()->getUserRepo()->findAllUsersWaitingForApproval();

        $data = [
            "users" => $users
        ];

        $view->render($data);
    }

    // Valide Pro
    public function validatePro ( ServerRequest $request ):void
    {
        $post_data = $request->getParsedBody();
        $users = RepositoryManager::getRm()->getUserRepo()->validatePro($post_data["id"]);

        header('Location: /admin/validatePro');
    }

    // Denied pro
    public function deniedPro ( ServerRequest $request ):void
    {
        $post_data = $request->getParsedBody();
        $users = RepositoryManager::getRm()->getUserRepo()->deniedPro($post_data["id"]);

        header('Location: /admin');
    }
}