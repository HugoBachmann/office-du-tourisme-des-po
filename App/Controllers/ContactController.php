<?php

namespace App\Controllers;

use App\Views\View;
use App\Forms\AntiCsrf;
use App\Forms\FormStatus;
use App\Session;
use App\RepositoryManager;
use Laminas\Diactoros\ServerRequest;

class ContactController extends Controller
{
    // CONTACT GET 
    public function contactFront() :void
    {
        $view = new View("contact");

        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get( Session::SESSION_FORM_STATUS )
        ];

        $view -> render($data);
    }

    // CONTACT POST 
    public function contactPost( ServerRequest $request) :void
    {
        $post_data = $request->getParsedBody();
        // var_dump($post_data);
        $this->csrfGuard( $post_data['csrf'] );

        $user = RepositoryManager::getRm()
            ->getContactRepo()
            ->contactPost(  $post_data[ 'user_name' ],  $post_data[ 'user_email' ], $post_data[ 'user_msg' ]);

        if( isset( $post_data[ 'user_name' ]) ) {

            header( 'Location: /' );
            die();
        } else {
            echo 'Remplisser tous les champs !';
            header( 'Location: /register' );
        }
    }

    // CONTACT LIST
    public function contactList () :void
    {
        $view = new View("contactAdmin");
        $view->template_only = true;
        $data = [
            'contacts' => RepositoryManager::getRm()->getContactRepo()->findAll()
        ];

        $view -> render($data);
    } 
}