<?php

namespace App\Controllers;

use App\RepositoryManager;
use App\Views\View;
use App\Session;
use Laminas\Diactoros\ServerRequest;

class UserController extends Controller
{
     // LIST MEMBRE : GET
    public function listMembre(): void
    {
        $view = new View("listeMembre");
        $view->template_only = true;
        $data = [
            'user' => RepositoryManager::getRm()->getUserRepo()->findAll()
        ];

        $view -> render($data);
    }

    // INFO MEMBRE : GET
    public function infoMembre( int $id): void
    {
        $view = new View("infoMembre");
        $view->template_only = true;
        $data = [
            'user' => RepositoryManager::getRm()->getUserRepo()->findById( $id )
        ];

        $view -> render($data);
    }  
}