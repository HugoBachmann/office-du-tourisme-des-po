<?php

namespace App\Controllers;

use App\Views\View;
use App\Forms\AntiCsrf;
use App\Forms\FormStatus;
use App\Session;
use App\RepositoryManager;
use Laminas\Diactoros\ServerRequest;

class PageController extends Controller
{
    // ACCUEIL
    public function index():void
    {
        $view = new View("home");

        $data = [
            "test" => "ceci est un test"
        ];

        $view -> render($data);
    }

    // BACKOFFICE
    public function admin():void
    {
        $view = new View("admin");

        $data = [
            "test" => "ceci est un test"
        ];

        $view -> render($data);
    }

    // CONTENT BACKOFFICE HOME
    public function infoAdmin():void
    {
        $view = new View("infoAdmin");
        $view->template_only = true;
        $data = [
            "test" => "ceci est un test"
        ];

        $view -> render($data);
    }

    // BACKOFFICE PRO
    public function backHost($id) :void
    {
        $view = new View("backHost");

        $data = [
             "rentals" => RepositoryManager::getRm()->getRentalRepo()->findAll()
        ];

        $view -> render($data);
    }



    // RETURN TABLE SQL IN ARRAY
    public function returnMapMarkers()
    {
        // On récupère notre liste de posts approuvés dans la bdd
        $mapMarkers = RepositoryManager::getRM()->getRentalRepo()->findAll();

        return json_encode($mapMarkers);
    }
}

