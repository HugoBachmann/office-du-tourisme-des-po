<?php

namespace App\Controllers;

use MiladRahimi\PhpRouter\Router;
use Laminas\Diactoros\Response\HtmlResponse;
use App\App;
use App\Database;
use App\Forms\AntiCsrf;
use App\Models\Dal;
use App\Session;

abstract class Controller
{
    public ?Router $router = null;

    public function __construct()
    {
        $this->router = App::getInstance()->getRouter();
    }

    protected function getCommonData(): array
    {
        $user = Session::get( Session::SESSION_USER );
        $is_auth = ! is_null( $user );
        $is_admin = $is_auth && $user->is_admin;

        return  [
            'is_auth' => $is_auth,
            'is_admin' => $is_admin,
            'user' => $user
        ];
    }

    public function csrfGuard( string $token ): void
    {
        if( AntiCsrf::checkToken( $token ) ) {
            return;
        }

        $this->router
            ->getPublisher()
            ->publish( new HtmlResponse( 'Demi-tour', 403 ) );

        die();
    }
}