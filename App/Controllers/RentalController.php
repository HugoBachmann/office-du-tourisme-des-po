<?php

namespace App\Controllers;

use Laminas\Diactoros\ServerRequest;

use App\RepositoryManager;
use App\Views\View;

use App\Forms\AntiCsrf;
use App\Forms\FormStatus;
use App\Repositories\RentalRepository;
use App\Repositories\USerRepository;
use App\Session;


class RentalController extends Controller
{
    // ADD RENT GET
    public function addRentals( ) :void
    {

        $view = new View( 'addRent' );
        $view->template_only = true;
        $user = Session::get( Session::SESSION_USER );

        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get( Session::SESSION_FORM_STATUS )
        ];

        $view->render( $data );
    }

    // ADD RENT POST
    public function addRentalsPost (ServerRequest $request ) :void
    {   
        $post_data = $request->getParsedBody();
        // var_dump($post_data);
        $this->csrfGuard( $post_data['csrf'] );

        $user = Session::get(Session::SESSION_USER);
        $attached_user_id = $user->id;

        if(!RepositoryManager::getRm()->getRentalRepo()->addRent($post_data['type'], $attached_user_id, $post_data['adresse'], $post_data['nbr_room'], $post_data['equipement'], $post_data[ 'description' ] , $post_data[ 'price' ], $post_data[ 'rent_request' ], $post_data[ 'rent_validate' ])) {
            
            header( 'Location: /' );
        } else {
            header( 'Location: /addRent' );
        }
    }

    // DELETE RENT POST
    public function deleteRent (ServerRequest $request) :void
    {
        $post_data = $request->getParsedBody();

        $rent = RepositoryManager::getRm()
        ->getRentalRepo()
        ->deleteRent( $post_data[ 'id' ]);

        if ( !is_null($rent) ) {
            echo 'Une erreur est survenu';
            
        } else {
            header( 'Location: /admin' );
            die();
        }
    }

    //UPDATE RENT
    public function updateRent(  ServerRequest $request ) :void
    {
        $post_data = $request->getParsedBody();
    
        $rent = RepositoryManager::getRm()->getRentalRepo()->updateRent($post_data[ 'type' ], $post_data[ 'owner_id' ], $post_data[ 'adresse' ], $post_data[ 'nbr_room' ], $post_data[ 'equipement' ], $post_data[ 'description' ] , $post_data[ 'price' ], $post_data[ 'rent_request' ], $post_data[ 'rent_validate' ],$post_data[ 'id' ],);
        
        if ( !is_null($rent) ) {
            echo 'Une erreur est survenu';
            
        } else {
            header( 'Location: /admin' );
            die();
        }
    }

    // UPDATE RENT USER

    // Liste des Rents
    public function listeRent(): void
    {
        $view = new View( 'listeRent' );
        $view->template_only = true;
        $data = [
            'rentals' => RepositoryManager::getRm()->getRentalRepo()->findAll()
        ];

        $view->render( $data );
    }

    // LISTE RENT FRONT (COMPTE RPO DOIT ETRE VALIDER )
    public function displayRent(): void
    {
        $view = new View( 'locations' );
        $data = [
            'rentals' => RepositoryManager::getRm()->getRentalRepo()->findAll()
        ];

        $view->render( $data );
    }

    // LOCATION UNIQUE (MODE VOIR PLUS)
    public function displayRentId($id): void
    {
        $view = new View("detailLocation");
        $data = [
            'rentals' => RepositoryManager::getRm()->getRentalRepo()->findById($id)
        ];

        $view -> render($data);
    }

    // Liste non-validateRent
    public function rentalRequest() :void
    {
        $view = new View("validateRent");
        $view->template_only = true;

        $rentals = RepositoryManager::getRm()->getRentalRepo()->findAllRentWaitingForApproval();

        $data = [
            "rentals" => $rentals
        ];

        $view->render($data);
    }

    // VALIDATE RENT
    public function validateRent ( ServerRequest $request ):void
    {
        $post_data = $request->getParsedBody();
        $rental = RepositoryManager::getRm()->getRentalRepo()->validateRent($post_data["id"]);

        header('Location: /admin');
    }

    // INFO RENT
    public function infoRent($id) :void
    {
        $view = new View("infoRent");
        $view->template_only = true;
        $data = [
            'rentals' => RepositoryManager::getRm()->getRentalRepo()->findById( $id )
        ];

        $view -> render($data);
    }

    // Détail d'une Rent
    public function show( int $id ): void
    {
        $view = new View( 'rentals-details' );

        $data = [
            'rental' => RepositoryManager::getRm()->getRentalRepo()->findById( $id )
        ];

        $view->render( $data );
    }
    
    // BACKOFFICE PRO
    public function rent($id) :void
    {
        $view = new View("rent");

        $data = [
             "rentals" => RepositoryManager::getRm()->getRentalRepo()->findById($id)
        ];

        $view -> render($data);
    }
}