<?php

namespace App;

class Utils
{
    public static function passwordHash( string $password ): string
    {
        return hash( 'sha512', SALT . $password . PEPPER );
    }
}