<?php 

namespace App\Models;

class Rental extends Model
{
    public string $type;
    public string $owner_id;
    public string $adresse;
    public int $nbr_room;
    public string $equipement;
    public string $description;
    public float $price;
    public bool $rent_request;
    public bool $rent_validate;
  

    public function getColumns(): array
    {
        return [ 'type', 'owner_id', 'adresse', 'nbr_room', 'equipement', 'description', 'price', 'rent_request', 'rent_validate' ];
    }
}