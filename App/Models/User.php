<?php

namespace App\Models;

class User extends Model
{
    public int $id;
    public string $email;
    public string $firstname;
    public string $lastname;
    public string $password;
    public bool $is_admin;
    public bool $is_host;
    public bool $pro_request;

    public function getColumns(): array
    {
        return ['id', 'email', 'firstname', 'lastname', 'password', 'is_admin', 'is_host', 'pro_request' ];
    }

    public function getFullName(): string
    {
        return sprintf( '%s %s', $this->firstname, $this->lastname );
    }
}