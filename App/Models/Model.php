<?php 

namespace App\Models;

use App\App;
use PDO;
use Laminas\Diactoros\Response\HtmlResponse;
use App\Database;

abstract class Model
{
    abstract public function getColumns():array;

    public int $id;
    
    public function __construct (array $data = [])
    {
        foreach( $data as $column => $value) {
            if( property_exists( get_called_class(), $column ) ) {
                $this->$column = $value;
            }
        }
    }

}