<?php

namespace App\Models;

class Contact extends Model
{
    public int $id;
    public string $user_email;
    public string $user_name;
    public string $user_msg;

    public function getColumns(): array
    {
        return ['id', 'user_email', 'user_name', 'user_msg'];
    }
}