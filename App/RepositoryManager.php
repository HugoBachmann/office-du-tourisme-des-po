<?php

namespace App;

use App\Repositories\RentalRepository;
use App\Repositories\UserRepository;
use App\Repositories\ContactRepository;

class RepositoryManager
{
    private static ?RepositoryManager $instance = null;

    // RENTAL REPO
    private RentalRepository $rentalRepo;
    public function getRentalRepo(): RentalRepository { return $this->rentalRepo; }

    // USER REPO
    private UserRepository $userRepo;
    public function getUserRepo(): UserRepository { return $this->userRepo; }

    // REPO
    private ContactRepository $contactRepo;
    public function getContactRepo(): ContactRepository { return $this->contactRepo; }

    public static function getRm(): ?self
    {
        if( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        $this->rentalRepo = new RentalRepository();
        $this->userRepo = new UserRepository();
        $this->contactRepo = new ContactRepository();
    }

    private function __clone() { }

    private function __wakeup() { }
}
