<?php



// Database
define( 'DB_HOST', 'mysql57' );
define( 'DB_NAME', 'database' );
define( 'DB_USER', 'mysql' );
define( 'DB_PASS', 'mysql' );

// Sécurité
define( 'SALT', 'un' );
define( 'PEPPER', 'deux' );

// Chemin fixe pour les styles / scripts

define( 'ASSETS_PATH', ROOT_PATH . 'Assets' . DS );
define( 'CSS_PATH', ASSETS_PATH . 'css' . DS );
define( 'JS_PATH', ASSETS_PATH . 'js' . DS );
define( 'IMG_PATH', ASSETS_PATH . 'img' . DS );