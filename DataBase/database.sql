-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql57
-- Généré le : lun. 05 avr. 2021 à 16:02
-- Version du serveur :  5.7.29
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `database`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `user_email` varchar(128) DEFAULT NULL,
  `user_msg` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `user_name`, `user_email`, `user_msg`) VALUES
(1, 'test', 'test', 'testestestest');

-- --------------------------------------------------------

--
-- Structure de la table `rental`
--

CREATE TABLE `rental` (
  `id` tinyint(4) NOT NULL,
  `type` varchar(64) DEFAULT NULL,
  `owner_id` int(10) DEFAULT NULL,
  `adresse` varchar(256) DEFAULT NULL,
  `nbr_room` int(5) DEFAULT NULL,
  `equipement` varchar(256) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `price` int(10) DEFAULT NULL,
  `rent_validate` tinyint(4) NOT NULL,
  `rent_request` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rental`
--

INSERT INTO `rental` (`id`, `type`, `owner_id`, `adresse`, `nbr_room`, `equipement`, `description`, `price`, `rent_validate`, `rent_request`) VALUES
(7, 'Maison à la location', 7, 'font-reomeu', 5, 'Tout équipé', '250 m² de vide montagnard', 250, 1, 0),
(8, 'Maison à la location', 7, 'Perpignan', 5, 'Liste des équipements', '10 m² trop vieux', 55, 0, 1),
(9, 'Appartement meublé', 21, 'Le soler', 2, 'Micro-onde', 'Parfait pour les cuisiniers', 66, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `is_host` tinyint(1) DEFAULT NULL,
  `pro_request` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `is_admin`, `is_host`, `pro_request`) VALUES
(7, 'admin', 'admin', 'admin@admin.com', 'c4b062892a90e4c693b900a688733b6cd5b983d25dc4f5b3545d6bd3ebb5bd907767837f568d070cfb779dd7d95b1bc56913ca61512acb3246716f8aa15495d4', 1, 0, 0),
(19, 'changer2', 'changer2', 'changer2@changer2', '2967385ad165c0638c2454080b7b9a6be876b14423363526439a1825681c783f4a9cc186d459541b33064f46f7dd88e5e232b1a40d196e41bbd5337e0c8afbb6', 0, 0, 0),
(21, 'pro', 'pro', 'pro@pro', 'd76ade7d9c6e8dc14878db64a5c2461b87dcdbdb0ae59fe9728237339bc5c15fbd4349af3b877416542d325c2eed81a45be94399beafd552f49718e3719cab5e', 0, 1, 0),
(22, 'Pierre', 'Rocher', 'pierre@rocher', '58d4c5c854810c1820c18ea4e32bd76ee980b3b74e3c0d2d14c9978ba44ad9dc4a5312b3546317ce1fedbe7fb805989decfd908d8cfd101575d38f4105bfb74b', 0, 0, 0),
(25, 'bru', 'bru', 'bruh@bruh', '85c6be320d891a3a216f63963c42c84f5240e36f33b1ed9d0a51f02c03bf5197ce1339db54058cb56a5556790fd2a2da5eed7a59e06dc798e04f0fbb024dd27e', 0, 0, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rental`
--
ALTER TABLE `rental`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `rental`
--
ALTER TABLE `rental`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
