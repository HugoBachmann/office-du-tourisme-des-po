<?php 

use App\App;

// Clean code
define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname( __FILE__ ) . DS );
define( 'APP_PATH', ROOT_PATH . DS . 'App' . DS );

//Config BDD
require_once ROOT_PATH . 'config.php';

//Autoload des classes
spl_autoload_register();

//Autoload du composer
require_once ROOT_PATH . 'vendor' . DS . 'autoload.php';

App::start();
